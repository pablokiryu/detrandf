﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeteErrosScript : MonoBehaviour
{
    public Sprite Correct;
    public SpriteRenderer spr;
    public SeteErrosController controller;
    public bool hasBeenFound = false;


    // Start is called before the first frame update
    void Start()
    {
        controller = SeteErrosController.Instance;
        spr = GetComponent<SpriteRenderer>();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        if (!hasBeenFound)
        {
            hasBeenFound = true;
            controller.found++;
            spr.sprite = Correct;
            //TODO: PLAYER FEEDBACK, som , particula etc;
           
        }
    }
}
