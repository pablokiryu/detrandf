﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class SeteErrosController : MonoBehaviour
{
    public int found;
    public static SeteErrosController Instance;
    public GameObject[] errors;
    public GameObject notFound;
    public UnityEngine.UI.Text txtContador;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        txtContador.text = Time.timeSinceLevelLoad.ToString("00");
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Pressed left click, casting ray.");
            CastRay();
        }

        if (found >= errors.Length)
        {
            //TODO: SETUP VICTORY PANEL/Screen
            //PLAY VICTORY SOUNDS>
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }

    void CastRay()
    {
        Vector3 vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        vec.z = 0;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (!hit)
        {
            //TODO: Tocar um som de errado.
            Instantiate(notFound, vec, Quaternion.identity);
        }
    }
}
