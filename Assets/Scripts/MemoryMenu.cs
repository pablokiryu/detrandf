﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryMenu : MonoBehaviour
{

    public void btnStart()
    {
        LoadScene("Memory_Game");
    }
    #region ButtonManagement
    
    public void BackBtnHandler()
    {
            LoadScene(1);     
    }
    public void LoadScene( int i )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(i);
    }
    public void LoadScene( string name )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
    #endregion
}
