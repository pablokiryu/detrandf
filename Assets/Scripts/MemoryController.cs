﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MemoryController : MonoBehaviour
{
    public GameObject slots;
    public List<GameObject> cards;
    public List<Sprite> sprites;
    public int[] deck = new int[] { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7 };
    public GameObject revealedCard = null;
    public bool canReveal = true;
    public int amntFoundCards =0;
    private IEnumerator hideCoroutine;
    public int tentativas = 0;

    public static MemoryController Instance;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in slots.transform)
        {
            cards.Add(child.gameObject);
        }
        deck.Shuffle();

        foreach (GameObject card in cards)
        {
            int index = cards.IndexOf(card);
            GameObject front = card.transform.GetChild(0).gameObject;
            Card cardScript = card.GetComponent<Card>();
            SpriteRenderer spr = front.GetComponent<SpriteRenderer>();
            cardScript.ID = deck[index];    
            spr.sprite = sprites[deck[index]];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (amntFoundCards >= sprites.Count)
        {
            //TODO: WIN THE GAME
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }



}

public static class ExtensionMethods
{
    private static System.Random rng = new System.Random();

    /// <summary>
    /// Implementation of the Fisher-Yates Shuffle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public static void Shuffle<T>( this IList<T> list )
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
