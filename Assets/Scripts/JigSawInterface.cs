﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JigSawInterface : MonoBehaviour
{
    public GameObject panelCiclovia, panelTorre;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //TODO: Refactor this mess, into an abstract class, or smth like that.
    #region ButtonManagement
    public void Ciclovia()
    {
        panelCiclovia.SetActive(true);
    }
    public void Torre()
    {
        panelTorre.SetActive(true);
    }
    public void BackBtnHandler()
    {
        if (panelCiclovia.activeSelf || panelTorre.activeSelf)
        {
            panelTorre.SetActive(false);
            panelCiclovia.SetActive(false);

        }
        else
        {
            LoadScene(1);
        }        
    }
    public void LoadScene( int i )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(i);
    }
    #endregion
}
