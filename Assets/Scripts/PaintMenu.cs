﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintMenu : MonoBehaviour
{
    public void LoadScene( string sceneName )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }
}
