﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Handles all things Main Menu
/// </summary>
public class MainMenuScript : MonoBehaviour
{
    public AudioMixer mixer;
    public float currVol;
    public GameObject soundbtn, mutebtn;
    public void Start()
    {
        mixer.GetFloat("MasterVol", out currVol);
        if (currVol !=  0)
        {
            mutebtn.SetActive(true);
            soundbtn.SetActive(false);
        }
        else
        {
            soundbtn.SetActive(true);
            mutebtn.SetActive(false);
        }
    }

    #region ButtonCallbacks
    /// <summary>
    /// Happens once "Inicializar" button is clicked
    /// </summary>
    public void ClickStartHandler()
    {
        SceneManager.LoadScene(1);
    }
    /// <summary>
    /// Happens once "Avaliar" button is clicked
    /// </summary>
    public void ClickRateHandler()
    {
    #if UNITY_ANDROID
        // TODO:  correct this URL to point to the correct APPID in the Market, before Release
        Application.OpenURL("market://details?id=YOUR_ID");
    //#elif UNITY_IPHONE
    //    Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
    #endif
    }
    /// <summary>
    /// Happens once "Sair" button is clicked
    /// </summary>
    public void ClickExitHandler()
    {

    #if UNITY_EDITOR
        //Stop playing the scene
        UnityEditor.EditorApplication.isPlaying = false;
    #endif
        //Quit the application
        Application.Quit();


    }
    /// <summary>
    /// Happens once "Som" button is clicked
    /// Should Mute the game
    /// </summary>
    public void ClickSoundHandler()
    {
        mixer.SetFloat("MasterVol", -80.0f);
        mutebtn.SetActive(true);
        soundbtn.SetActive(false);

    }   
    /// <summary>
    /// Happens once "Mute" button is clicked
    /// Should UnMute the game
    /// </summary>
    public void ClickMuteHandler()
    {
        mixer.SetFloat("MasterVol", 0.0f);
        soundbtn.SetActive(true);
        mutebtn.SetActive(false);
    }
    #endregion
}
