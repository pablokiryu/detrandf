﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Paintable : MonoBehaviour
{
    public SpriteRenderer spr;
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
    } 

    void OnMouseDown()
    {
        spr.color = PaintController.Instance.setColor;
        
    }
}
