﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceScript : MonoBehaviour
{
    public bool released = true;
    public bool isFitIn = false;

    //void OnEnable()
    //{
    //   JigSawController.Instance.pieceObjs.Add(this.gameObject);
    //}
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    private Vector2 screenPoint;
    private Vector2 offset;

    public void OnMouseDown()
    {
        if (!isFitIn)
        {
            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            released = false;
        }
    }
    public void OnMouseUp()
    {
        released = true;
        //TODO: Particle Effects, and maybe sound too. 
    }

    public void OnMouseDrag()
    {
        if (!isFitIn)
        {

            Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
            transform.position = curPosition + offset;

        }
    }
}