﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    public int ID = 0;
    public bool hasBeenFound = false;
    MemoryController controller;
    // Start is called before the first frame update
    void Start()
    {
        controller = MemoryController.Instance;
    }
    public void OnMouseDown()
    {
        
        if (!hasBeenFound && controller.canReveal)
        {

            //TODO: add player feedback to this whole section, sounds particles, animations etc.
            if (controller.revealedCard == null && controller.revealedCard != this.gameObject)
            {
                this.transform.rotation = Quaternion.Euler(0, 180, 0);
                controller.revealedCard = this.gameObject;
            }
            else if (controller.revealedCard == this.gameObject)
            {
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
                controller.revealedCard = null;
            }
            else
            {
                Card otherCard = controller.revealedCard.GetComponent<Card>();
                this.transform.rotation = Quaternion.Euler(0, 180, 0);
                if (ID == otherCard.ID)
                {
                    controller.amntFoundCards++;
                    controller.revealedCard = null;
                    hasBeenFound = true;
                    otherCard.hasBeenFound = true;
                    //TODO: ADD FEEDBACK, play a sound on the controller and particles,
                    //indicating you found the correct pieces
                   
                }else
                {
                    //TODO: WAIT A SECOND , maybe a sec and half
                    //THEN: 
                    controller.canReveal = false;
                    controller.revealedCard = null;
                    StartCoroutine(reHideCards(new Card[] { this, otherCard }));
                    controller.tentativas++;
                }

            }
        }
    }
    IEnumerator reHideCards( Card[] cards )
    {
        yield return new WaitForSeconds(1f);
        cards[0].transform.rotation = Quaternion.Euler(0, 0, 0);
        cards[1].transform.rotation = Quaternion.Euler(0, 0, 0);
        controller.canReveal = true;
    }


}
