﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JigSawController : MonoBehaviour
{
    public List<GameObject> pieceObjs;
    public List<GameObject> slotObjs;
    public GameObject pieces;
    public GameObject slots;
    public int numberOfPieces;
    public int fitPieces;
    public static JigSawController Instance;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        foreach (Transform child in pieces.transform)
        {
            pieceObjs.Add(child.gameObject);
        }

        foreach (Transform child in slots.transform)
        {
            slotObjs.Add(child.gameObject);
        }

        for (int i = 0; i < slotObjs.Count; i++)
        {
            slotObjs[i].GetComponent<SlotScript>().slotPiece = pieceObjs[i];
        }
        numberOfPieces = pieceObjs.Count;

        //TODO: Randomize pieces starting position , outside of the Correct image, and 
    }
    // Update is called once per frame
    void Update()
    {
        // I wonder if maybe this should goto JigsawInterface.cs
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackBtnHandler();
        }
        if ( (fitPieces == numberOfPieces) && fitPieces != 0)
        {
            //TODO: DISPLAY CORRECT IMAGE, play sounds and particles.

            //TODO: WIN FEEDBACK
            //maybe enable panel, that has buttons that go back to previuous screen and Play again?
            //for now  we just revert back to the previous level
            BackBtnHandler();
        }
    }

    #region ButtonManagement
    public void BackBtnHandler()
    {
        LoadScene(2);
    }
    void LoadScene( int i )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(i);
    }
    #endregion

}
