﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameMenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LoadScene(0);
        }
        
    }

    public void BackBtnHandler()
    {
        LoadScene(0);
    }

    public void GameSceneBtnHandler(string sceneName)
    {
        LoadScene(sceneName);
    }


    void LoadScene(int i)
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(i);
    }

    //TODO: Refactor this mess, into an abstract class, or smth like that.
    void LoadScene(string sceneName )
    {
        PlayerPrefs.SetInt("previousLevel", UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }
}
