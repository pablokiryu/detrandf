﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Brush : MonoBehaviour
{
    Vector3 startPos,newPos;
    public Color thisColor;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        startPos = this.transform.position;
        newPos = this.transform.position + offset;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseOver()
    {
        transform.position = newPos;
       // Debug.Log("MouseOver on: " + this.gameObject.name);
    }
    void OnMouseDown()
    {

        PaintController.Instance.setColor = this.thisColor;
       // Debug.Log("MouseDown on: "+this.gameObject.name);
    }
    void OnMouseExit()
    {
        transform.position = startPos;
        //Debug.Log("MouseExit on: " + this.gameObject.name);
    }
}
