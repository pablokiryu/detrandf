﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class DragScript : MonoBehaviour
{
    private Vector2 screenPoint;
    private Vector2 offset;

    public void OnMouseDown()
    {

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
    }

   public void OnMouseDrag()
    {
        Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition+offset;
    }

}