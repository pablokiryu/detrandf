﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotScript : MonoBehaviour
{
    
    public GameObject slotPiece;
    public PieceScript pcScript;
    public float distance;
    public float threshold;

    // Start is called before the first frame update
    void Start()
    {
        pcScript = slotPiece.GetComponent<PieceScript>();
    }

    // Update is called once per frame
    void Update()
    {
       distance = Vector2.Distance(this.transform.position, slotPiece.transform.position);
        if (distance < threshold && pcScript.released && !pcScript.isFitIn )
        {
            //TODO: animate the piece going in
            //TODO: give feedback to player ( particles, sound, etc..)
            //for now :
            slotPiece.transform.position = this.transform.position;
            pcScript.isFitIn = true;
            JigSawController.Instance.fitPieces++;
        }
    }
}
